package com.sgo.saldomu.coreclass;/*
  Created by Administrator on 10/13/2014.
 */

import java.util.HashMap;

public interface FragmentCommunicator {
    void respond(HashMap<String, String> data, Boolean flag);
}
